const path = require('path');
const htmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: './src/Background.js',
  output: {
    filename: 'Bundle.js',
    path: path.resolve(__dirname, 'dist'),
  },
  devServer: { 
      contentBase: path.join(__dirname, 'dist'),
      compress: true, 
      port: 4001
    },
  plugins: [
    new htmlWebpackPlugin({
      filename: 'index.html',
      template: './src/index.html' 
    })
   ] 
};