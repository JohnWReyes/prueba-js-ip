 
// class containing the run function
// Use the api ipify.org 

class Background {
    static run() {
      fetch("https://api.ipify.org?format=json")
        .then((response) => response.json())
        .then((data) => {
            const { ip } = data;
            document.getElementById('message').innerHTML=ip;
        });
    }
  }
  
  Background.run();